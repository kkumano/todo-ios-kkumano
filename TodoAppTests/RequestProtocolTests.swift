//
//  RequestProtocolTests.swift
//  TodoAppTests
//
//  Created by kkumano on 2020/07/31.
//  Copyright © 2020 kkumano. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class RequestProtocolTests: XCTestCase {

    func testInit() {
        let request = TestRequestProtocol()

        XCTAssertNil(request.parameters)
        XCTAssertEqual(request.baseUrl, "https://sonix-todo-api-kkumano.herokuapp.com/")
        XCTAssertEqual(request.headers?["Content-Type"], "application/json")
        XCTAssertEqual(request.encoding.toJsonEncoding(), JSONEncoding.default)
        XCTAssertEqual(request.method, .get)
        XCTAssertEqual(request.path, "todos")
    }
}
