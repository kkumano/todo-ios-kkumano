//
//  TestRequestProtocol.swift
//  TodoAppTests
//
//  Created by kkumano on 2020/07/31.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Alamofire
@testable import TodoApp

class TestRequestProtocol: RequestProtocol {
    typealias Response = TodosGetResponse
    
    var path: String {
        return "/todos"
    }
    var method: HTTPMethod {
        return .get
    }
    var parameters: Parameters? {
        return nil
    }
    var baseUrl: String {
        return "https://sonix-todo-api-kkumano.herokuapp.com/"
    }
    var encoding: ParameterEncoding {
        return JSONEncoding.default
    }
    var headers: HTTPHeaders? {
        return ["Content-Type": "application/json"]
    }
}

extension ParameterEncoding {
    func toJsonEncoding() -> JSONEncoding? {
        self as? JSONEncoding
    }
}

extension JSONEncoding: Equatable {
    public static func == (lhs: JSONEncoding, rhs: JSONEncoding) -> Bool {
        return lhs.options == rhs.options
    }
}
