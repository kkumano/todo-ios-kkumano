//
//  TodoPutRequestTests.swift
//  TodoAppTests
//
//  Created by kkumano on 2020/07/31.
//  Copyright © 2020 kkumano. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPutRequestTests: XCTestCase {
    let todo = Todo(id: 1, title: "更新しました", detail: nil, date: nil)
    
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = TodoPutRequest(todo: todo)
        
        XCTAssertEqual(request.method, .put)
        XCTAssertEqual(request.path, "todos/1")
    }
    
    func testResponse() {
        var todoPutResponse: CommonResponse?
        
        stub(condition: isHost("https://sonix-todo-api-kkumano.herokuapp.com/")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Put Todo Request")
        
        APIClient().call(
            request: TodoPutRequest(todo: todo),
            success: { response in
                todoPutResponse = response
                expectation.fulfill()
        }, failure: { _ in
            return
        })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoPutResponse)
            XCTAssertEqual(todoPutResponse?.errorCode, 0)
            XCTAssertEqual(todoPutResponse?.errorMessage, "")
        }
    }
}
