//
//  TodoPostRequestTests.swift
//  TodoAppTests
//
//  Created by kkumano on 2020/07/31.
//  Copyright © 2020 kkumano. All rights reserved.
//

import XCTest
import OHHTTPStubs
@testable import TodoApp

final class TodoPostRequestTests: XCTestCase {
    let body = TodoRequestBody(title: "test4", detail: nil, date: nil)
    
    override func tearDown() {
        HTTPStubs.removeAllStubs()
        super.tearDown()
    }
    
    func testInit() {
        let request = TodoPostRequest(body: body)
        
        XCTAssertEqual(request.method, .post)
        XCTAssertEqual(request.path, "todos")
    }
    
    func testResponse() {
        var todoPostResponse: CommonResponse?
        
        stub(condition: isHost("https://sonix-todo-api-kkumano.herokuapp.com/")) { _ in
            return HTTPStubsResponse(
                fileAtPath: OHPathForFile("CommonResponse.json", type(of: self))!,
                statusCode: 200,
                headers: ["Content-Type": "application/json"]
            )
        }
        
        let expectation = self.expectation(description: "Post Todo Request")
        
        APIClient().call(
            request: TodoPostRequest(body: body),
            success: { response in
                todoPostResponse = response
                expectation.fulfill()
        }, failure: { _ in
            return
        })
        
        waitForExpectations(timeout: 10) { _ in
            XCTAssertNotNil(todoPostResponse)
            XCTAssertEqual(todoPostResponse?.errorCode, 0)
            XCTAssertEqual(todoPostResponse?.errorMessage, "")
        }
    }
}
