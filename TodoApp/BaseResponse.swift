//
//  BaseResponse.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/20.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Foundation

protocol BaseResponse: Codable {
    var errorCode: Int { get }
    var errorMessage: String { get }
}
