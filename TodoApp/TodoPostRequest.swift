//
//  TodoPostRequest.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/20.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Alamofire

struct TodoPostRequest: RequestProtocol {
    typealias Response = CommonResponse

    let body: TodoRequestBody

    var path: String {
        return "todos"
    }
    var method: HTTPMethod {
        return .post
    }
    var parameters: Parameters? {
        var parameters = ["title": body.title]
        if let detail = body.detail {
            parameters["detail"] = detail
        }
        if let date = body.date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            parameters["date"] = formatter.string(from: date)
        }
        return parameters
    }
}
