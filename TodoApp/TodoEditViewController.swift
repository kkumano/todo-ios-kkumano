//
//  TodoEditViewController.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/15.
//  Copyright © 2020 kkumano. All rights reserved.
//

import UIKit

protocol TodoEditViewControllerDelegate: class {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String)
}

final class TodoEditViewController: UIViewController {
    var todo: Todo?
    weak var delegate: TodoEditViewControllerDelegate?
    
    private let titleMaxLength = 100
    private let detailMaxLength = 1000
    private var titleCount: Int {
        titleTextField.text?.count ?? 0
    }
    private var detailCount: Int {
        detailTextView.text.count
    }
    private var isTitleLimitOver: Bool {
        titleCount > titleMaxLength
    }
    private var isDetailLimitOver: Bool {
        detailCount > detailMaxLength
    }
    private let datePicker = UIDatePicker()

    private let dateFormatter = DateFormatter()

    @IBOutlet private weak var registrationButton: UIButton!
    @IBOutlet private weak var titleTextField: UITextField!
    @IBOutlet private weak var titleCountLabel: UILabel!
    @IBOutlet private weak var detailCountLabel: UILabel!
    @IBOutlet private weak var dateTextField: UITextField!
    @IBOutlet private weak var detailTextView: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItem()
        setUpDetailTextView()
        setUpDatePicker()
        dateFormatter.dateFormat = "yyyy/M/d"
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        if let todo = todo {
            setUpTexts(todo: todo)
            loadTextCount()
        }
        setUpRegistrationButton()
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
    }

    private func setUpRegistrationButton() {
        if todo != nil {
            registrationButton.setTitle("更新", for: .normal)
        }
        registrationButton.setTitleColor(.white, for: .normal)
        registrationButton.backgroundColor = .gray
        registrationButton.layer.borderWidth = 0.5
        registrationButton.layer.borderColor = UIColor.black.cgColor
        registrationButton.layer.cornerRadius = 10.0
        switchRegistrationButton()
    }

    private func setUpDetailTextView() {
        detailTextView.layer.borderColor = UIColor.gray.cgColor
        detailTextView.layer.borderWidth = 0.5
        detailTextView.layer.cornerRadius = 5.0
        detailTextView.layer.masksToBounds = true
        detailTextView.delegate = self
    }

    private func loadTextCount() {
        titleCountLabel.text = "\(titleCount)"
        detailCountLabel.text = "\(detailCount)"
    }

    @IBAction private func titleFieldCounted(_ sender: Any) {
        titleCountLabel.text = "\(titleCount)"
        if isTitleLimitOver {
            titleCountLabel.textColor = .red
        } else {
            titleCountLabel.textColor = .black
        }
        switchRegistrationButton()
    }

    private func switchRegistrationButton() {
        if isTitleLimitOver || titleCount == 0 || isDetailLimitOver {
            registrationButton.backgroundColor = .gray
            registrationButton.isEnabled = false
        } else {
            registrationButton.backgroundColor = .blue
            registrationButton.isEnabled = true
        }
    }

    @IBAction private func onClickRegistrationButton(_ sender: Any) {
        guard let title = titleTextField.text else { return }
        let detail = detailTextView.text.isEmpty ? nil : detailTextView.text
        let dateText = dateTextField.text ?? ""
        let date = dateFormatter.date(from: dateText)
        let todoRequestBody = TodoRequestBody(title: title, detail: detail, date: date)
        sendTodo(body: todoRequestBody)
    }

    private func sendTodo(body: TodoRequestBody) {
        if let id = todo?.id {
            updateTodo(id: id, body: body)
        } else {
            createTodo(body: body)
        }
    }

    private func createTodo(body: TodoRequestBody) {
        APIClient().call(
            request: TodoPostRequest(body: body),
            success: { [weak self] _ in
                // 取得成功時の処理
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "登録しました")
            },
            failure: { [weak self] message in
                // 失敗時の処理
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func updateTodo(id: Int, body: TodoRequestBody) {
        let todo = Todo(id: id, title: body.title, detail: body.detail, date: body.date)
        APIClient().call(
            request: TodoPutRequest(todo: todo),
            success: { [weak self] _ in
                // 取得成功時の処理
                guard let self = self else { return }
                self.delegate?.todoEditViewControllerDidSendTodo(self, message: "更新しました")
            },
            failure: { [weak self] message in
                // 失敗時の処理
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func setUpDatePicker() {
        datePicker.calendar = .init(identifier: .gregorian)
        datePicker.datePickerMode = .date
        datePicker.timeZone = NSTimeZone.local
        datePicker.locale = .current
        dateTextField.inputView = datePicker

        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 35))
        let spaceItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneItem = UIBarButtonItem(title: "決定", style: .done, target: self, action: #selector(doneButtonTapped))
        let cancelItem = UIBarButtonItem(title: "閉じる", style: .plain, target: self, action: #selector(cancelButtonTapped))
        let trashItem = UIBarButtonItem(title: "削除", style: .plain, target: self, action: #selector(deleteButtonTapped))
        toolbar.setItems([trashItem, spaceItem, cancelItem, doneItem], animated: true)

        dateTextField.inputView = datePicker
        dateTextField.inputAccessoryView = toolbar
    }

    @objc private func doneButtonTapped() {
        dateTextField.endEditing(true)
        dateTextField.text = dateFormatter.string(from: datePicker.date)
    }

    @objc private func cancelButtonTapped() {
        dateTextField.endEditing(true)
    }

    @objc private func deleteButtonTapped() {
        dateTextField.endEditing(true)
        dateTextField.text = ""
    }

    private func setUpTexts(todo: Todo) {
        titleTextField.text = todo.title
        detailTextView.text = todo.detail
        if let date = todo.date {
            dateTextField.text = dateFormatter.string(from: date)
        }
    }

    // タイトル記入時、改行しようとした際にソフトウェアキーボードが閉じる
    @IBAction func returnKeyTapped(_ sender: Any) {
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}

extension TodoEditViewController: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        detailCountLabel.text = "\(detailCount)"
        if isDetailLimitOver {
            detailCountLabel.textColor = .red
        } else {
            detailCountLabel.textColor = .black
        }
        switchRegistrationButton()
    }
}
