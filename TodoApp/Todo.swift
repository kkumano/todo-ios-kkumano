//
//  Todo.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/20.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Foundation

struct Todo: Codable {
    let id: Int
    let title: String
    let detail: String?
    let date: Date?
}
