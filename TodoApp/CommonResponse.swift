//
//  CommonResponse.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/20.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Foundation

struct CommonResponse: BaseResponse {
    let errorCode: Int
    let errorMessage: String
}
