//
//  TodoPutRequest.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/21.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Alamofire

struct TodoPutRequest: RequestProtocol {
    typealias Response = CommonResponse

    let todo: Todo

    var path: String {
        return "todos/\(todo.id)"
    }
    var method: HTTPMethod {
        return .put
    }
    var parameters: Parameters? {
        var parameters = ["title": todo.title, "detail": todo.detail, "date": nil]
        if let date = todo.date {
            let formatter = DateFormatter()
            formatter.timeZone = TimeZone(identifier: "UTC")
            formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
            formatter.locale = Locale(identifier: "en_US_POSIX")
            parameters["date"] = formatter.string(from: date)
        }
        return parameters as Parameters
    }
}
