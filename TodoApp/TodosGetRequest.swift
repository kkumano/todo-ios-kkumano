//
//  TodosGetRequest.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/20.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Alamofire

struct TodosGetRequest: RequestProtocol {
    typealias Response = TodosGetResponse

    var parameters: Parameters? {
        return nil
    }
    var path: String {
        return "todos"
    }
    var method: HTTPMethod {
        return .get
    }
}
