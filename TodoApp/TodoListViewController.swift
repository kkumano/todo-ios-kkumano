//
//  TodoListViewController.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/14.
//  Copyright © 2020 kkumano. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialSnackbar

final class TodoListViewController: UIViewController {
    private var todos: [Todo] = []
    private var isDeleteMode = false
    @IBOutlet private weak var todoTableView: UITableView!
    @IBOutlet private weak var composeBarButtonItem: UIBarButtonItem!

    @IBAction private func onTappedComposeBarButtonItem(_ sender: UIBarButtonItem) {
        isDeleteMode = false
        updateTrashBarButtonItem()
        transitionToEditView()
    }

    @IBAction private func onTappedTrashBarButtomItem(_ sender: UIBarButtonItem) {
        isDeleteMode.toggle()
        updateTrashBarButtonItem()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setUpNavigationItem()
        todoTableView.dataSource = self
        todoTableView.delegate = self
        fetchTodoList()
    }

    private func setUpNavigationItem() {
        navigationItem.title = "TODO APP"
        navigationItem.backBarButtonItem = UIBarButtonItem()
    }

    private func fetchTodoList() {
        APIClient().call(
            request: TodosGetRequest(),
            success: { [weak self] result in
                // 取得成功時の処理
                self?.todos = result.todos
                self?.todoTableView.reloadData()
            },
            failure: { [weak self] message in
                // 失敗時の処理
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func deleteTodo(id: Int) {
        APIClient().call(
            request: TodoDeleteRequest(id: id),
            success: { [weak self] _ in
                // 取得成功時の処理
                self?.fetchTodoList()
            },
            failure: { [weak self] message in
                // 失敗時の処理
                self?.showErrorAlert(message: message)
            }
        )
    }

    private func showSnackbar(text: String) {
        let message = MDCSnackbarMessage()
        message.text = text
        MDCSnackbarManager.show(message)
    }

    private func transitionToEditView(todo: Todo? = nil) {
        let storyboard = UIStoryboard(name: "Edit", bundle: nil)
        guard let todoEditViewController = storyboard.instantiateInitialViewController() as? TodoEditViewController else { return }
        todoEditViewController.delegate = self
        todoEditViewController.todo = todo
        navigationController?.pushViewController(todoEditViewController, animated: true)
    }

    private func updateTrashBarButtonItem() {
        let systemItem: UIBarButtonItem.SystemItem = isDeleteMode ? .reply : .trash
        let trashBarButtonItem = UIBarButtonItem(barButtonSystemItem: systemItem, target: self, action: #selector(onTappedTrashBarButtomItem(_:)))
        navigationItem.rightBarButtonItems = [trashBarButtonItem, composeBarButtonItem]
    }

    private func showDeleteTodoAlert(todo: Todo) {
        let alertController = UIAlertController(title: "「\(todo.title)」を削除します。", message: "よろしいですか？", preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "キャンセル", style: .cancel))
        let okButtonAction = UIAlertAction(title: "OK", style: .default) { _ in
            self.deleteTodo(id: todo.id)
        }
        alertController.addAction(okButtonAction)
        present(alertController, animated: true)
    }
}

extension TodoListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = todos[indexPath.row].title
        return cell
    }
}

extension TodoListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let todo = todos[indexPath.row]
        tableView.deselectRow(at: indexPath, animated: true)
        if isDeleteMode {
            showDeleteTodoAlert(todo: todo)
        } else {
            transitionToEditView(todo: todo)
        }
    }
}

extension TodoListViewController: TodoEditViewControllerDelegate {
    func todoEditViewControllerDidSendTodo(_ todoEditViewController: TodoEditViewController, message: String) {
        navigationController?.popViewController(animated: true)
        showSnackbar(text: message)
        fetchTodoList()
    }
}
