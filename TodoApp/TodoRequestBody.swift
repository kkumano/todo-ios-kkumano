//
//  TodoRequestBody.swift
//  TodoApp
//
//  Created by kkumano on 2020/07/20.
//  Copyright © 2020 kkumano. All rights reserved.
//

import Foundation

struct TodoRequestBody {
    let title: String
    let detail: String?
    let date: Date?
}
